public class Polynomial {
	private int[] koeficient;
	private int stupeň;

	public Polynomial(int a, int b) {
		koeficient = new int[b + 1];
		koeficient[b] = a;
		stupeň = degree();
	}

	public Polynomial(Polynomial p) {
		koeficient = new int[p.koeficient.length];
		for (int i = 0; i < p.koeficient.length; i++) {
			koeficient[i] = p.koeficient[i];
		}

		stupeň = p.degree();
	}

	public int getCoefficient(int stupeň) {
		return 0;
	}

	public int koeficent() {
		return coeff(degree());
	}

	private int degree() {

		return 0;
	}

	@SuppressWarnings("null")
	public int coeff(int degree) {
		if (degree > this.degree())
			throw new RuntimeException("CHYBA");
		int[] koeficient = null;
		return koeficient[degree];
	}

	public int degree1() {
		int d = 0;
		for (int i = 0; i < koeficient.length; i++)
			if (koeficient[i] != 0)
				d = i;
		return d;
	}

	public String toString() {
		if (stupeň == 0)
			return "" + koeficient[0];
		if (stupeň == 1)
			return koeficient[1] + "x + " + koeficient[0];
		String m = koeficient[stupeň] + "x^" + stupeň;
		for (int i = stupeň - 1; i >= 0; i--) {
			if (koeficient[i] == 0) {
				continue;
			} else if (koeficient[i] > 0) {
				m = m + " + " + (koeficient[i]);
			} else if (koeficient[i] < 0)
				m = m + " - " + (-koeficient[i]);
			if (i == 1) {
				m = m + "x";
			} else if (i > 1)
				m = m + "x^" + i;
		}
		return m;
	}

	static int maximum(int x, int y) {
		return (x > y) ? x : y;
	}

	static int[] add(int A[], int B[], int x, int y) {
		int velikost = maximum(x, y);
		int sum[] = new int[velikost];

		for (int i = 0; i < x; i++) {
			sum[i] = A[i];
		}
		for (int i = 0; i < y; i++) {
			sum[i] = B[i];
		}
		return sum;
	}

	static void printPolynomial(int Polynomial[], int y) {
		for (int i = 0; i < y; i++) {
			System.out.print(Polynomial[i]);
			if (i != 0) {
				System.out.print("x^" + 1);
			}
			if (i != y - 1) {
				System.out.print(" + ");
			}
		}
	}

	static int[] multiply(int A[], int B[], int x, int y) {

		int[] product = new int[x + y - 1];

		for (int i = 0; i < x + y - 1; i++) {

			product[i] = 0;
		}

		for (int i = 0; i < x; i++) {

			for (int f = 0; f < y;) {

				product[i + f] += A[i] * B[f];
			}
		}

		return product;

	}

	static void PrintPolymonial(int Polynomial[], int y) {

		for (int i = 0; i < y; i++) {

			System.out.print(Polynomial[i]);
			if (i != 0) {
				System.out.print("x^" + 1);
			}
			if (i != y - 1) {
				System.out.print(" + ");
			}
		}

	}
}

//Spolupráce s David Heliman a Jan Sklenička
